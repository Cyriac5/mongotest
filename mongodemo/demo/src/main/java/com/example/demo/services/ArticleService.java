package com.example.demo.services;

import com.example.demo.ArticleRepository;
import com.example.demo.model.Article;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ArticleService{
    private final ArticleRepository articleRepository;

    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Optional<Article> findByTitle( String title){
        return articleRepository.findByTitle(title);
    }

    public List<Article> findAll(){
        return articleRepository.findAll();
    }

    public void deleteArticle(String articleId){
        articleRepository.deleteById(articleId);
    }

    public Article createArticle(String title, String content, String summary){
        Article newArticle = new Article(UUID.randomUUID().toString(),title, content, summary);
        articleRepository.save(newArticle);
        return newArticle;
    }

    public Optional<Article> findById(String articleId){
        return articleRepository.findById(articleId);
    }
}

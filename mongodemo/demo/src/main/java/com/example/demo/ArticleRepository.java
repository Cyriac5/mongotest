package com.example.demo;

import com.example.demo.model.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ArticleRepository extends MongoRepository<Article, String> {

    public Optional<Article> findByTitle( String title);
}

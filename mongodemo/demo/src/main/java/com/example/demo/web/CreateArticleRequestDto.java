package com.example.demo.web;

public record CreateArticleRequestDto (String title, String content, String summary){

}

package com.example.demo.web;

import com.example.demo.ArticleRepository;
import com.example.demo.model.Article;
import com.example.demo.services.ArticleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/articles")
@CrossOrigin
public class ArticleController {

    private final ArticleService articleService;

    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @PostMapping
    ResponseEntity<Article> createArticle(@RequestBody CreateArticleRequestDto body) {
        Article createdArticle = articleService.createArticle(body.title(), body.content(), body.summary());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdArticle);
    }

    @GetMapping
    List<Article> displayArticle (){
        return articleService.findAll();
    }

    @DeleteMapping("/{articleId}")
    @ResponseStatus(HttpStatus.OK)
    void deleteArticle(@PathVariable String articleId){
        articleService.deleteArticle(articleId);
    }

    @GetMapping("/{articleId}")
    ResponseEntity<Article> findArticle(@PathVariable String articleId){
        return articleService.findById(articleId).map(u -> ResponseEntity.ok(u)).orElseGet(() -> ResponseEntity.notFound().build());
    }




}
